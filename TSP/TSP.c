// TSP program using by Branch&Bound Algorithm
// 4/4/2018.

#include <stdio.h>
//#include <conio.h>
#include <math.h>

#define MAX_INFINITE	99999
// number of cities
#define NUM_CITY	7

// position of cities (2D coordinate)
int pos[NUM_CITY][2] = { {0, 0},
						{2, 6},
						{8, 4},
						{7, 2},
						{1, 6},
						{4, 9},
						{3, 2} };

double	dist[NUM_CITY][NUM_CITY];	// 2D array of distance between of cities

int		queue[NUM_CITY];	// array of queue
int		stack[NUM_CITY];	// array of stack
int		other[NUM_CITY];	// array of other
int		solut[NUM_CITY];	// array of solution

// distance between neighbor cities
#define NEIGHBOR_DISTANCE	dist[stack[pointer]][stack[pointer - 1]]
#define TOSTART_DISTANCE	dist[stack[pointer]][stack[0]]

void initializematrix()
{
	int i, j;

	// Set the dist matrix (Euclid distance between cities)
	for (i = 0; i < NUM_CITY; i++)
	{
		queue[i] = i;

		for (j = i + 1; j < NUM_CITY; j++)
		{
			dist[i][j] = (double)sqrt((pos[i][0] - pos[j][0]) * (pos[i][0] - pos[j][0]) + (pos[i][1] - pos[j][1]) * (pos[i][1] - pos[j][1]));
			dist[j][i] = dist[i][j];
		}

		printf("%1d: ( %1d, %1d )", i, pos[i][0], pos[i][1]);

		printf("\n");
	}

}
int main()
{
	int i;
	double short_dist = MAX_INFINITE, curr_dist = 0;
	int pointer = 0, pos_top = 0, pos_bottom = NUM_CITY - 1;

	initializematrix();

	stack[0] = queue[0];
	other[0] = 0;

	for(;;)
	{
		while (pointer < NUM_CITY - 1 && curr_dist < short_dist)
		{
			pointer++;
			pos_top++;

			if (pos_top == NUM_CITY)
				pos_top = 0;

			stack[pointer] = queue[pos_top];
			curr_dist += NEIGHBOR_DISTANCE;
			other[pointer] = NUM_CITY - pointer - 1;
		}

		// If current solution (distance) is smaller than short_dist, short_dist change to one.
		if (curr_dist + TOSTART_DISTANCE < short_dist)
		{
			short_dist = curr_dist + TOSTART_DISTANCE;

			for (i = 0; i < NUM_CITY; i++)
				solut[i] = stack[i] + 1;
		}

		// If there is no more branches 
		while (other[pointer] == 0 && pointer >= 0)
		{
			if (++pos_bottom == NUM_CITY)
				pos_bottom = 0;

			queue[pos_bottom] = stack[pointer];
			curr_dist -= NEIGHBOR_DISTANCE;
			pointer--;
		}

		// If stack pointer is smaller than 0, break
		if (pointer < 0)
			break;

		if (++pos_bottom == NUM_CITY)
			pos_bottom = 0;

		queue[pos_bottom] = stack[pointer];
		curr_dist -= NEIGHBOR_DISTANCE;

		// Search in other branch
		other[pointer] = other[pointer] - 1;

		if (++pos_top == NUM_CITY) 
			pos_top = 0;

		stack[pointer] = queue[pos_top];
		curr_dist += NEIGHBOR_DISTANCE;
	}

	// output result
	printf("shortest distance = %.1f\n", short_dist);

	printf("shortest path : ");
	for (i = 0; i < NUM_CITY; i++) 
		printf("%d -> ", solut[i]);

	printf("%d\n", stack[0] + 1);

	//getch();
	getchar();

	return 0;
}